\section{Configuration Spaces}
Configuration space is denoted by $C$, and elements in the configuration space are shown as $q$ ($q \in C$): these elements are everything that we need in order to know where the robot is.

\textbf{Example 1:}
If we have an $x-y$ plane on which the robot can translate, the configuration space is as follows:

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.2]{robot_xy}
  \caption{A robot $R$ that moves in x and y.}
\end{figure}
 
$q=(x,y)$, $A$ is a function that takes a configuration and gives the robot position.
$A: C\rightarrow P(w)$ where $P$ is a power set and $W$ denotes the world in $\mathbb{R}^2$ or $\mathbb{R}^3$.
$A(q)$ represents every point $x$ that is in the world and can be occupied by the robot in position $q$. It can be defined as the following set (shown as Figure~\ref{fig:Aq_translate}):

\begin{equation*}
A(q)=\{x|x \in W, x \text{ occupied by the robot in } q\}
\end{equation*}

\textbf{Example 2:}
When a robot can both be translated and rotated in the space with 3 degrees of freedom (DOF), $q=(x,y,\theta)$, $A(q)$ is shown as Figure~\ref{fig:Aq_rotate}.

\textbf{Example 3:}
When a robot arm can rotate with 2DOF, $q=(\theta_1,\theta_2)$, $A(q)$ is shown as Figure~\ref{fig:Aq_2dof}.

\textbf{Note:} Usually robot arms have 6/7 degrees of freedom when considering their elbow and a wrist.

\begin{figure}[h]
  \centering
  \begin{subfigure}{0.3\textwidth}
    \centering
    \includegraphics[scale=0.3]{Aq_translate}
    \caption{$A(q)$ when a robot translates in x-y.}
    \label{fig:Aq_translate}
  \end{subfigure}
  \begin{subfigure}{0.3\textwidth}
    \centering
    \includegraphics[scale=0.3]{Aq_rotate}
    \caption{$A(q)$ when a robot translates and rotates in x-y.}
    \label{fig:Aq_rotate}
  \end{subfigure}
  \begin{subfigure}{0.3\textwidth}
    \centering
    \includegraphics[scale=0.3]{Aq_2dof}
    \caption{$A(q)$ when for a robot arm that rotates.}
    \label{fig:Aq_2dof}
  \end{subfigure}
  \caption{$A(q)$ with different robot behaviors.}
\end{figure}

\subsection{A World with Obstacles}

Real worlds are usually encountered with obstacles. We denote obstacles by $O$ ($O \subset W$) and the obstacle region in the configuration space by $C_{obs}$,
\begin{equation*}
C_{obs}=\{ q \in C| A \subset q \wedge O \neq \emptyset\}
\end{equation*}

and $C_{free} = C \setminus C_{obs}$ is where we can keep the robot.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.2]{obs}
  \caption{A robot $R$ that moves in x and y with an obstacle $O$.}
\end{figure}

\textbf{Note:} $C_{obs}$ is a closed set because it contains its limit points. Limit points are the edges of the configuration space or the touching configurations. Touching in $C_{free}$ is not allowed and it is an open set.

Figure~\ref{fig:Cobs} shows the configuration space for a robot when considering an obstacle.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.3]{Cobs}
  \caption{$C_{obs}$ for a robot $R$ that translates in x-y with a rectangle obstacle $O$}
  \label{fig:Cobs}
\end{figure}

% Relation between R,W and points and C

\subsection{Minkowski Difference}

\begin{equation*}
C_{obs}=O \ominus A(\emptyset) = \{ o-a| o \in O, a \in A\},
\end{equation*}

where $A(\emptyset)$ is $A$ at the origin (what happens if robot is in the origin?), $\ominus$ is the Minkowski difference between all possible pairs of $o \in O$ and $a\in A$.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.35]{minkowski}
  \caption{Minkowski difference}
  \label{fig:Minkowski}
\end{figure}

\textbf{Theorem:} If $O$ and $A(\emptyset$) are convex, then $C_{obs}$ is convex.

\textbf{Proof:} All lines joining points in a convex segment lie inside the segment.
\begin{eqnarray*}
&&\forall t_1, t_2 \in C_{obs}, \quad \forall \lambda \in [0,1] , \quad A'=-A \text{ reflected around the origin}\\
&&t_1=o_1+a'_1,\quad t_2=o_2+a'_2\\
&&O \text{: convex } \rightarrow \lambda o_1 +(1-\lambda)o_2 \in O (1)\\
&&A' \text{: convex} \rightarrow \lambda a'_1+(1-\lambda)a'_2 \in A'(2)\\
&&(1),(2) \rightarrow t_1 ,t_2 \in \text{ union of } O \text{ and } A'
\end{eqnarray*}

\subsection{Considering Translations and Rotations}

If we have translations and rotations possible for the robot, then the obstacle can be represented in a 3-D space as a volume and depending on a particular orientation, obstacle can be represented as one of the layers in the volume:

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{obs_rotation}
  \caption{Considering translations and rotations}
\end{figure}

For an arm with one degree of freedom, $C_{obs}$ looks like as:

\begin{figure}[h]
  \centering
  \begin{subfigure}{0.6\textwidth}
    \centering
    \includegraphics[scale=0.3]{arm_1dof_obs}
    \caption{A robot arm with 1DOF and one obstacle.}
    \label{fig:rotate_1dof}
  \end{subfigure}
  \begin{subfigure}{0.3\textwidth}
    \centering
    \includegraphics[scale=0.3]{arm_1dof_Cobs}
    \caption{$C_{obs}$ for this case.}
    \label{fig:cobs_1dof}
  \end{subfigure}
  \caption{$C_{obs}$ considering an arm with 1DOF.}
\end{figure}

For an arm with two degrees of freedom, $C$ is similar to a donut shape and $C_{obs}$ is some part of that as:

\begin{figure}[h]
  \centering
  \begin{subfigure}{0.6\textwidth}
    \centering
    \includegraphics[scale=0.3]{arm_2dof_obs}
    \caption{A robot arm with 2DOF and one obstacle.}
    \label{fig:rotate_2dof}
  \end{subfigure}
  \begin{subfigure}{0.3\textwidth}
    \centering
    \includegraphics[scale=0.3]{arm_2dof_Cobs}
    \caption{$C_{obs}$ for this case.}
    \label{fig:cobs_2dof}
  \end{subfigure}
  \caption{$C_{obs}$ considering an arm with 2DOF.}
\end{figure}


When the robot can grasp an object or touches an object, there will be an additional link attached to the robot and we need to worry about how object intersects with the world as well as the robot. This additional link can affect $C_{obs}$ eventually.

If we have a 2-linked planar arm, $C_{obs}$ looks like:

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{arm_planner_Cobs}
  \caption{$C_{obs}$ for a 2-linked planner robot arm.}
\end{figure}